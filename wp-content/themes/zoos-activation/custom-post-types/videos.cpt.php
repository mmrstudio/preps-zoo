<?php

    // define cpt
    $cpt_labels = array(
        'post_type_name' => 'video',
        'singular' => 'Video',
        'plural' => 'Videos',
        'slug' => 'videos'
    );

    // define cpt options
    $cpt_options = array(
        'supports' => array('title'),
        'rewrite' => array(
            'slug' => 'video',
            'pages' => false,
            'with_front' => false,
        ),
        'has_archive' => false
    );

    // create cpt
    $videos_cpt = new CPT($cpt_labels, $cpt_options);

    // set dashicon
    $videos_cpt->menu_icon('dashicons-format-video');
