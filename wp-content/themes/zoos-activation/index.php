<?php

if(have_posts()) while (have_posts()) : the_post();

    $get_activity_tiles = get_field('activity_tiles');
    $activity_tiles = [];

    foreach($get_activity_tiles as $tile) :

        $activity_tiles[] = [
            'title' => $tile['title'],
            'image' => acf_image($tile['image']),
            'link' => $tile['link'],
        ];

    endforeach;

    get_header();

?>

<script>
    window.adBannerUrl = '<?php echo acf_image(get_field('ad_banner')); ?>';
    window.facemaskUrl = '<?php echo get_field('facemask'); ?>';
    window.activityTiles = <?php echo json_encode($activity_tiles); ?>;
</script>

<div id="app"></div>
<div class="incompatible" id="incompatible">
    <div class="incompatible__inner">
        <div class="incompatible__text">
            <p class="title">Please upgrade your browser</p>
            <p class="description">Unfortunately your browser doesn't support this site.<br>Please use one of the following broswers:<br>Google Chrome, Firefox, Safari, Internet Explorer 11 or Microsoft Edge</p>
        </div>
    </div>
</div>

<?php

    get_footer();

endwhile; // end loop
