<?php

    date_default_timezone_set('Australia/Melbourne');

    // Composer autoload
    //require_once ABSPATH . '../vendor/autoload.php';

    // Load extra helper functions
    require_once (TEMPLATEPATH . '/includes/helper_functions.php');
    require_once (TEMPLATEPATH . '/includes/ajax_functions.php');

    // Set default CONSTANTS
    define( 'SITE_NAME' , get_bloginfo('name') );
    define( 'THEME_URL' , get_template_directory_uri() );
    define( 'PAGE_BASENAME' , get_current_basename() );
    define( 'FIRST_VISIT' , is_first_time());
	define( 'HOME_URL' , get_permalink(get_page_by_path('home')) );

    // Load custom post types
    require_once (TEMPLATEPATH . '/custom-post-types/CPT.php');
    require_once (TEMPLATEPATH . '/custom-post-types/videos.cpt.php');

    // Add theme supports
    add_theme_support('post-thumbnails');

    // Register navigation
    register_nav_menu( 'main-nav' , 'Main Menu' );

    // Add image sizes
    add_image_size('small', 500 , 0 , false );

    // Set default image link type
    update_option('image_default_link_type', 'file');

    // Set JPG quality for image resizing
    add_filter( 'jpeg_quality', 'set_jpg_quality' );
    function set_jpg_quality( $quality ) { return 95; }

    // Disabled admin bar
    add_filter('show_admin_bar', '__return_false');

    // Load stylesheets and javascript files
    add_action('init', 'theme_init');

    add_filter('gform_confirmation_anchor', '__return_false');

    // Initilization function
    function theme_init() {

        // Load stylesheets and javascript files
        load_scripts_and_styles();

        // Create options page
        create_theme_options_page();

        // hide stuff we don't need
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
        remove_action('wp_head', 'wp_generator');
        remove_action('wp_head', 'index_rel_link');
        remove_action('wp_head', 'feed_links');

        // Remove the REST API endpoint.
        remove_action('rest_api_init', 'wp_oembed_register_route');

        // Turn off oEmbed auto discovery.
        // Don't filter oEmbed results.
        remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);

        // Remove oEmbed discovery links.
        remove_action('wp_head', 'wp_oembed_add_discovery_links');

        // Remove oEmbed-specific JavaScript from the front-end and back-end.
        remove_action('wp_head', 'wp_oembed_add_host_js');

        remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
        remove_action( 'wp_print_styles', 'print_emoji_styles' );

    }


    function load_scripts_and_styles() {

        // not admin or login/register screens
        if( ! is_admin() && ! in_array($_SERVER['PHP_SELF'], ['/wp-login.php', '/wp-register.php'])) :

            // Add stylesheets
            wp_enqueue_style( 'theme' , THEME_URL . '/style.css' , FALSE , '0.1.0' , 'screen' );
            wp_enqueue_style( 'main_styles' , THEME_URL . '/dist/bundle.css' , FALSE , '0.1.0' , 'screen' );

            // Load scripts
            add_action('wp_enqueue_scripts', 'load_scripts', 0);

            // Replace jQuery with Google CDN version
            function load_scripts() {

                // Load jQuery from CDN
                if(! is_admin()) :
                    $jquery_version = '3.2.1';
                    wp_deregister_script( 'jquery' );
                    wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/' . $jquery_version . '/jquery.min.js' , array() , $jquery_version , $jq_in_footer );
                    wp_enqueue_script( 'jquery' );
                endif;

                // Load main scripts
                wp_enqueue_script( 'main_script' , THEME_URL . '/dist/bundle.js' , [] , '0.1.0' , TRUE );

            }

        endif;

    }

    function create_theme_options_page() {

        if( function_exists('acf_add_options_page') ) {

        	$page = acf_add_options_page(array(
        		'page_title' 	=> 'Options',
        		'menu_title' 	=> 'Options',
        		'menu_slug' 	=> 'options',
        		'capability' 	=> 'edit_posts',
        		'redirect' 	=> false
        	));

        }

    }

    // Remove hardcoded width/height attrs from post thumbnails
    add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3 );
    function remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
        $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
        return $html;
    }

    // Remove dimension attributes from img tags in content
    add_filter('the_content', 'remove_img_dimensions', 10);
    function remove_img_dimensions($html) {
        $html = preg_replace('/(width|height)=["\']\d*["\']\s?/', "", $html);
        return $html;
    }

    // Add nav menu item names to links
    add_filter( 'nav_menu_css_class', 'add_nav_item_name_class', 10, 2 );
    function add_nav_item_name_class( $classes , $item ) {
        $new_class = strtolower( preg_replace("/[^A-Za-z0-9 ]/", '-', $item->title) );
        $new_class = str_replace(' ', '-', $new_class);
        //$classes[] = $new_class;
        $classes[] = $new_class.'-link';
        return $classes;
    }

    // Add post/page slug to body classes (eg. page-home)
    add_filter( 'body_class', 'add_slug_body_class' );
    function add_slug_body_class( $classes ) {
        global $post;
        if (isset($post)) $classes[] = $post->post_type . '-' . $post->post_name;
        return $classes;
    }


    function getParam($param, $default='') {
        return isset($_GET[$param]) ? $_GET[$param] : $default;
    }

    function utmParams() {
        $utmParams = 'utm_source=mmr&amp;utm_medium=' . getParam('utm_medium', 'direct') . '&amp;utm_campaign=30_days_of_giveaways';
        if(getParam('utm_content', false)) $utmParams .= '&amp;utm_content=' . getParam('utm_content', false);
        return $utmParams;
    }
