<?php

/* Template Name: Puzzle */

if(have_posts()) while (have_posts()) : the_post();

    get_header();

?>

<script>
    window.themeUrl = '<?php echo THEME_URL; ?>';
</script>

<div id="puzzle"></div>
<div class="incompatible" id="incompatible">
    <div class="incompatible__inner">
        <div class="incompatible__text">
            <p class="title">Please upgrade your browser</p>
            <p class="description">Unfortunately your browser doesn't support this site.<br>Please use one of the following broswers:<br>Google Chrome, Firefox, Safari, Internet Explorer 11 or Microsoft Edge</p>
        </div>
    </div>
</div>

<?php

    get_footer();

endwhile; // end loop
