<?php

	### HELPER FUNCTIONS ###

	// Function to build HTML5 data attributes (eg. <tag data-key="value">)
	function build_html_data_attributes( $data_arr=array() ) {
		$data_processed_array = array();
		foreach( $data_arr as $key=>$val ) : $data_processed_array[] = 'data-'.$key.'="'.$val.'"'; endforeach;
		return implode(' ', $data_processed_array);
	}

	// Function get current URL basename
	function get_current_basename() {
		$url = explode('/', 'http://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
		$base = $url[3];
		return $base;
	}

	// Function to get image attachment ID from its link
	function get_image_id_by_link($link) {
		global $wpdb;
		$link = preg_replace('/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $link);
		return $wpdb->get_var("SELECT ID FROM {$wpdb->posts} WHERE guid='$link'");
	}

	// Content navigation
	function _posts_archive_nav( $prev_str='&larr; Older posts', $next_str='Newer posts &rarr;' )
		{
			global $wp_query;

			if ( $wp_query->max_num_pages > 1 ) : ?>
				<nav class="content_nav">
					<div class="nav-previous"><?php next_posts_link($prev_str); ?></div>
					<div class="nav-next"><?php previous_posts_link($next_str); ?></div>
				</nav>
			<?php endif;
		}

	// Next/Prev post navigation
	function _post_nav( $prev_str='%link', $next_str='%link' )
		{
			global $wp_query;

		?>
				<nav class="post_nav">
					<div class="nav-previous"><?php previous_post_link($prev_str, '&larr; Previous'); ?></div>
					<div class="nav-next"><?php next_post_link($next_str, 'Next &rarr;'); ?></div>
				</nav>
			<?php
		}

	// Read more link
	add_filter('excerpt_more', 'new_excerpt_more');
	function new_excerpt_more($more) { return '... <br />'; }

	// Check if iOS device
	function is_iOS() {
		if (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') ||
			strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone') ||
			strpos($_SERVER['HTTP_USER_AGENT'], 'iPod')) :
			return TRUE;
		else :
			return FALSE;
		endif;
	}

	// Check for H1 tag
	function content_has_h1($content)
		{
			$has_h1 = FALSE;
			if (preg_match_all("=<h1[^>]*>(.*)</h1>=siU", $content, $matches)) {
				$has_h1 = TRUE;
			}
			return $has_h1;
		}

	// Simple BB code parser

	function bb($text) {
		$array = array(
			'[b]' => '<strong>',
			'[/b]' => '</strong>',
			'[i]' => '<em>',
			'[/i]' => '</em>',
			'[u]' => '<span class="underline">',
			'[/u]' => '</span>',
			'[center]' => '<span class="center">',
			"[/center]" => "</span>",
			'[left]' => '<span class="left">',
			"[/left]" => "</span>",
			'[right]' => '<span class="right">',
			"[/right]" => "</span>"
		);

		$newtext = str_replace( array_keys($array) , array_values($array) , $text );
		return $newtext;
	}


	// New line to paragraphs

	function nl2p($html,$indentation='') {
		$html = preg_replace("/(\r\n|\n|\r)/", "\n", $html);
		$html = preg_replace("/\n\n+/", "\n\n", $html);
		$html = preg_replace('/\n?(.+?)(\n\n|\z)/s', $indentation."<p>$1</p>", $html);
		$html = preg_replace('|(?<!</p> )\s*\n|', "<br />", $html);
		$html = str_replace('</p>','</p>'."\n",$html);
		return $html;
	}

    function truncate_text($text, $length) {
        return (strlen($text) >= $length) ? substr($text, 0, ($length-3)) . '...' : $text;
    }


	// Truncate text to number of words specified

	function limit_words($str,$num_words=50,$suffix='...') {
		$str_array = explode(' ',$str);
		$return_str = '';

		for($w=0; $w<$num_words; $w++) {
			$return_str .= $str_array[$w].' ' ;
		}

		if(count($str_array) > $num_words) {
			$return_str .= $suffix;
		}

		return $return_str ;
	}


	function first_paragraph($text, $isHTML = true) {

		$result = $text;

		if( $isHTML ) :

			// convert line breaks/paragraphs
			$result = str_replace("\n", "", $result); // remove excess \n
			$result = str_replace("<br>", "\n", $result);
			$result = str_replace("<br/>", "\n", $result);
			$result = str_replace("<br />", "\n", $result);
			$result = str_replace("</p>", "\n\n", $result);

			// strip all remaining tags
			$result = strip_tags($result);

		endif;

		// try and return the first paragraph, if I can't, return all of it
		$paragraphs = explode("\n\n", trim($result));

		if(count($paragraphs) > 1) :
			return nl2br(trim($paragraphs[0]));
		else :
			return $text;
		endif;

	}

	// Function to load ACF gallery in post
    function acf_gallery($gallery, $size='full', $thumb=false, $thumb_2x=false) {

        $gallery_images = array();

        if( $gallery && is_array($gallery) ) :

            //print_r($gallery); //exit;

            foreach( $gallery as $image ) :

                $gallery_image = array(
                    'id' => $image['id'],
                    'title' => $image['title'],
                    'alt' => $image['alt'],
                );

                if($size == 'full') :
                    $gallery_image['url'] = $image['url'];
                    $gallery_image['w'] = $image['width'];
                    $gallery_image['h'] = $image['height'];
                else :
                    $gallery_image['url'] = $image['sizes'][$size];
                    $gallery_image['w'] = $image['sizes'][$size . '-width'];
                    $gallery_image['h'] = $image['sizes'][$size . '-height'];
                endif;

                if($thumb) :
                    $gallery_image['thumb'] = $image['sizes'][$thumb];
                    $gallery_image['thumb_w'] = $image['sizes'][$thumb . '-width'];
                    $gallery_image['thumb_h'] = $image['sizes'][$thumb . '-height'];
                endif;

                if($thumb_2x) :
                    $gallery_image['thumb-2x'] = $image['sizes'][$thumb_2x];
                endif;

                $gallery_image['full_url'] = $image['url'];

                $gallery_image['img'] = '<img src="' . $gallery_image['url'] . '" alt="' . $gallery_image['alt'] . '" title="' . $gallery_image['title'] . '">';

                $gallery_images[] = $gallery_image;

            endforeach;

        endif;

        //print_r($gallery_images); exit;

        return $gallery_images;

    }

    function acf_image($image_field=false, $size=false, $img_tag=false, $img_attrs=false) {

        if(! $image_field) return false;

        if(! $size) :
            $image = $image_field['url'];
        else :
            $image = isset($image_field['sizes'][$size]) ? $image_field['sizes'][$size] : $image_field['url'];
        endif;

        if($img_tag) :

            $attrs_str = '';
            $attrs = array( 'src' => $image );

            if($image_field['alt']) $attrs['alt'] = $image_field['alt'];
            if($image_field['title']) $attrs['title'] = $image_field['title'];

            if($img_attrs && is_array($img_attrs)) :
                $attrs = array_merge($attrs, $img_attrs);
            endif;

            foreach($attrs as $attr_key => $attr_val) :
                $attrs_str .= ' ' . $attr_key . '="' . $attr_val . '"';
            endforeach;

            $image = '<img '.$attrs_str.'>';

        endif;

        return $image;

    }

    function acf_repeater($repeater_key, $callback, $field_source=false) {

        $i = 0;

        if( have_rows($repeater_key, $field_source) ) :
            while ( have_rows($repeater_key, $field_source) ) : the_row();
                $callback($i);
                $i++;
            endwhile;
        endif;

    }

    function acf_flexible($flexible_key, $callback) {

        $i = 0;

        if( have_rows($flexible_key) ) :
            while ( have_rows($flexible_key) ) : the_row();
                $callback($i, get_row_layout());
                $i++;
            endwhile;
        endif;

    }

	// Get file size function
	function get_file_size($file) {
		$file_size = '';
		if(is_file($file)) :
			$file_size = filesize($file);
			if($file_size < 1024) :
				$file_size = $file_size.'B';
			elseif($file_size >= 1024 AND $file_size < 1048576) :
				$file_size = intval(($file_size / 1024)). 'KB';
			elseif($file_size >= 1048576) :
				$file_size = intval(($file_size / 1048576)).'MB';
			endif;
		endif;

		return $file_size;
	}

	// Prints copyright range (eg. 2007-2011)
	function copyright_range($first_year, $separator) {
		$range = '';
		$current_year = date("Y");
		if($first_year < $current_year) :
			$range = $first_year.$separator.$current_year;
		else :
			$range = $current_year;
		endif;
		return $range;
	}

	// Add additional class to body class from outside header.php
	function add_body_class($additional_class) {
		global $add_additional_class;
		$add_additional_class = $additional_class;
		// Add specific CSS class by filter
		add_filter('body_class', 'add_additional_class');

		if( ! function_exists('add_additional_class')) :
			function add_additional_class($classes) {
				global $add_additional_class;
				// add 'class-name' to the $classes array
				$classes[] = $add_additional_class;
				// return the $classes array
				return $classes;
			}
		endif;
	}

	// Replace tags in footer text
	function write_footer_text($text) {
		$text = str_replace('[C]', '&copy', $text);
		$text = str_replace('[year]', date('Y'), $text);
		return $text;
	}


	function sibling_pages_menu( $post_parent ) {

		wp_list_pages(array(
			'depth'        => '2',
			'child_of'     => $post_parent,
			'title_li'     => '',
			'echo'         => 1,
			'sort_column'  => 'menu_order, post_title',
			'post_type'    => 'page',
			'post_status'  => 'publish'
		));

	}

	// List of Australian States
	function australian_states() {
		return array(
			'ACT' => 'Australian Capital Territory',
			'NSW' => 'New South Wales',
			'NT'  => 'Northern Territory',
			'QLD' => 'Queensland',
			'SA'  => 'South Australia',
			'TAS' => 'Tasmania',
			'VIC' => 'Victoria',
			'WA'  => 'Western Australia'
		);
	}

	function google_analytics( $key='', $url='auto' ) {

		return "
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '".$key."', '".$url."');
  ga('send', 'pageview');

</script>
		";

	}

	function theme_file( $file ) {
		return THEME_URL . '/' . $file;
	}

	function twitterify($ret) {
		$ret = preg_replace("#(^|[\n ])([\w]+?://[\w]+[^ \"\n\r\t< ]*)#", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $ret);
		$ret = preg_replace("#(^|[\n ])((www|ftp)\.[^ \"\t\n\r< ]*)#", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);
		$ret = preg_replace("/@(\w+)/", "<a href=\"http://www.twitter.com/\\1\" target=\"_blank\">@\\1</a>", $ret);
		$ret = preg_replace("/#(\w+)/", "<a href=\"https://twitter.com/search?q=\\1\" target=\"_blank\">#\\1</a>", $ret);
		return $ret;
	}


	function first_post_image() {
		global $post, $posts;
		$first_img = '';
		ob_start();
		ob_end_clean();
		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
		$first_img = $matches[1][0];
		if(empty($first_img)) {
			$first_img = FALSE;
		}
		return $first_img;
	}

    function removeEmoji($text) {

        $clean_text = "";

        // Match Emoticons
        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $clean_text = preg_replace($regexEmoticons, '', $text);

        // Match Miscellaneous Symbols and Pictographs
        $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $clean_text = preg_replace($regexSymbols, '', $clean_text);

        // Match Transport And Map Symbols
        $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
        $clean_text = preg_replace($regexTransport, '', $clean_text);

        return $clean_text;
    }


    function output_nav($location='main-nav', $depth=1) {

        wp_nav_menu( array(
            'menu_class' => 'nav-' . $location,
            'container' => 'none',
            'theme_location' => $location,
            'depth' => $depth
        ));

    }


    function isAjax() {
        return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
    }

    function is_first_time() {
        if(session_id() == '') session_start();
        if (isset($_SESSION['_wp_first_time'])) return false;
        $_SESSION['_wp_first_time'] = true;
        return true;
    }

    function get_page_title() {

        if( is_front_page() ) :
            return get_bloginfo('name');
        else :
            return wp_title( '|', false, 'right' ) . get_bloginfo('name');
        endif;

    }

    function parse_youtube_id($url) {
        parse_str( parse_url( $url, PHP_URL_QUERY ), $video_url );
        return $video_url['v'];
    }

	function get_post_terms($post_id, $taxonomy='category') {

		$post_terms = array(
			'get_terms' => get_the_terms($post_id, $taxonomy),
			'terms' => array()
		);

		foreach($post_terms['get_terms'] as $term) :

			$post_terms['terms'][] = array(
				'id' => $term->term_id,
				'name' => $term->name,
				'slug' => $term->slug,
				'permalink' => get_term_link($term->term_id, $taxonomy),
			);

		endforeach;

		foreach($post_terms['terms'] as $term) :

			$post_terms['names'][] = $term['name'];

		endforeach;

		foreach($post_terms['terms'] as $term) :

			$post_terms['slugs'][] = $term['slug'];

		endforeach;

		foreach($post_terms['terms'] as $term) :

			$post_terms['links'][] = '<a href="' . $term['permalink'] . '">' . $term['name'] . '</a>';

		endforeach;

		//print_r($post_terms); exit;

		return $post_terms;

	}

    function curl_post($url, $params) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params, null, '&'));
        $ret = curl_exec($ch);
        curl_close($ch);
        return $ret;
    }

    function get_fb_page_token($app_id, $secret) {
        $url = 'https://graph.facebook.com/oauth/access_token';
        $token_params = array(
            "type" => "client_cred",
            "client_id" => $app_id,
            "client_secret" => $secret
        );
        return str_replace('access_token=', '', curl_post($url, $token_params));
    }

    function toCamelCase($string) {
        $string = sanitize_title($string);
        $string = explode('-', $string);
        $string = array_map(function($val) {
            return ucfirst($val);
        }, $string);
        return lcfirst(implode($string));
    }


    function hex2rgb($hex = "#000000") {
    	$f = function($x) { return hexdec($x); };
    	return array_map($f, str_split(str_replace("#", "", $hex), 2));
    }

    function rgb2hex($rgb = array(0, 0, 0)) {
    	$f = function($x) { return str_pad(dechex($x), 2, "0", STR_PAD_LEFT); };
    	return "#" . implode("", array_map($f, $rgb));
    }

    function mixColor($color_1 = array(0, 0, 0), $color_2 = array(0, 0, 0), $weight = 0.5) {
    	$f = function($x) use ($weight) { return $weight * $x; };
    	$g = function($x) use ($weight) { return (1 - $weight) * $x; };
    	$h = function($x, $y) { return round($x + $y); };
    	return array_map($h, array_map($f, $color_1), array_map($g, $color_2));
    }

    function tintColor($color, $weight = 0.5) {
    	$t = $color;
    	if(is_string($color)) $t = hex2rgb($color);
    	$u = mixColor($t, array(255, 255, 255), $weight);
    	if(is_string($color)) return rgb2hex($u);
    	return $u;
    }

    function shadeColor($color, $weight = 0.5) {
    	$t = $color;
    	if(is_string($color)) $t = hex2rgb($color);
    	$u = mixColor($t, array(0, 0, 0), $weight);
    	if(is_string($color)) return rgb2hex($u);
    	return $u;
    }
