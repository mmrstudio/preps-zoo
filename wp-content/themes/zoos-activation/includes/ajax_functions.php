<?php

    function add_ajax_action($action, $callback) {
        add_action('wp_ajax_'.$action, $callback);
        add_action('wp_ajax_nopriv_'.$action, $callback);
    }

    function request_param($key, $default) {
        return (isset($_REQUEST[$key])) ? $_REQUEST[$key] : $default ;
    }

    function json_response($data) {
        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }

    add_ajax_action('generateVideo', function() {

        //print_r($_POST); exit;

        $to = request_param('to', false);
        $from = request_param('from', false);
        $videoName = 'To' . trim(ucfirst(strtolower($to))) . 'From' . trim(ucfirst(strtolower(str_replace(' ' , '', $from))));

        $existing_video = get_page_by_title($videoName, 'OBJECT', 'video');

        if($existing_video) :
            $videoUrl = get_field('url', $existing_video->ID);
            return json_response($videoUrl);
        endif;

        $existing_video = new WP_Query([
            'numberposts' => -1,
            'post_type' => 'video',
            'post_status' => 'publish',
            'meta_key' => 'reference',
            'meta_value' => $reference,
        ]);

        $videoUrl = false;
        $videoRequestUrl = 'https://balancer.creativa.com.au/201709-mmr-victoriazoo/?name_to=' . urlencode($to) . '&name_from=' . urlencode($from) . '&GUID=' . urlencode($videoName) . '&sectionid=sc1,sc2,sc3,sc4&audiosectionid=1';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $videoRequestUrl);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        if (preg_match('~Location: (.*)~i', $result, $match)) {
            $videoUrl = trim($match[1]);
        }

        $video_id = wp_insert_post([
            'post_title' => $videoName,
            'post_type' => 'video',
            'post_status' => 'publish',
        ]);

        // save data
        update_field('field_59f66ad71e967', $to, $video_id); // to
        update_field('field_59f66caeb0023', $from, $video_id); // from
        update_field('field_59f66adc1e968', $videoUrl, $video_id); // url

        return json_response($videoUrl);
    });

    add_ajax_action('sendFriend', function() {

        $me = request_param('me', false);
        $friends = request_param('friends', false);

        if($me) :
            list($sender, $sender_email) = explode(',', $me);
        else :
            $sender = false;
        endif;

        if($friends) {
            $friends = explode(';', $friends);
        }

        if($sender == false || $friends == false) return json_response(false);

        $emailBody = file_get_contents(TEMPLATEPATH . '/friendEmail.php');

        foreach($friends as $friend) {

            list($name, $email) = explode(',', $friend);

            $body = $emailBody;
            $body = str_replace('{{ $name }}', $name, $body);
            $body = str_replace('{{ $sender }}', $sender, $body);
            $body = str_replace('{{ $sender_email }}', $sender_email, $body);

            $subject = 'There\'s a letter for you from the animals at the zoo';
            $headers = ['Content-Type: text/html; charset=UTF-8'];

            wp_mail($email, $subject, $body, $headers);
        }

        return json_response(true);
    });
